<?php
// $Id: devel.admin.inc,v 1.6 2011/01/02 14:13:20 salvis Exp $

function reflect_admin_settings() {
  $form['email_notifications'] = array(
    '#type' => 'fieldset',
    '#title' => 'Email notifications',
    '#description' => t('After which Reflect actions should email notifications be sent out?'),
  );
  
  $form['email_notifications']['reflect_notify_commenter_of_summary'] = array(
    '#type' => 'checkbox',
    '#title' => 'Notify commenter',
    '#description' => 'Email the commenter when someone summarizes one of their points',
    '#default_value' => variable_get('reflect_notify_commenter_of_summary', TRUE)
  );

  $form['email_notifications']['reflect_notify_summarizer_of_response'] = array(
    '#type' => 'checkbox',
    '#title' => 'Notify summarizer',
    '#description' => 'Email the user who added a summary bullet point when a commenter responds to it.',
    '#default_value' => variable_get('reflect_notify_summarizer_of_response', TRUE)
  );

  return system_settings_form($form);
}