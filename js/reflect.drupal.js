(function($) {
  // Re-enable form elements that are disabled for non-ajax situations.
  Drupal.behaviors.reflect = {
    attach: function() {

      Drupal.settings.reflect = {
        domain : 'drupal_module',
      	media_dir: Drupal.settings.basePath + Drupal.settings.reflect.path + '/media',
        reflectPath: Drupal.settings.basePath + Drupal.settings.reflect.path, // These variables are new and not yet integrated.
        servicesPath: Drupal.settings.basePath  + 'reflect/', // These variables are new and not yet integrated.
      };
      var $j = jQuery.noConflict();

      Reflect.config.study = false;
      Reflect.config.view.enable_rating = true;
      
      $j.extend(Reflect.config.api, {
        domain : 'drupal_module',
      	media_dir: Drupal.settings.reflect.media_dir //these settings need to be stored in Reflect.config.api... - Travis
      });      
      
      $j.extend(Reflect.config.contract, {
      	components: [{
          comment_identifier: '.comment',
          comment_offset:7,
          comment_text:'.drupal_comment_wrapper .content',
          get_commenter_name: function(comment_id){return $j.trim($j('#comment'+comment_id+ ' .username').text());}
        }]
      });

      Reflect.Contract = Reflect.Contract.extend({
      	user_name_selector : function(){return '';},
      	modifier: function(){
      	    $j('.comment').each(function(){
      	       var id = $j(this).prev('a').attr('id').substring(8);
                 $j(this).attr('id','comment'+id);
               $j(this).children().wrapAll('<div class="drupal_comment_wrapper"></div>');
      	    });
      	},
      	get_comment_thread: function(){
      	    return $j('.region-content');
      	}
      });

      Reflect.api.DataInterface = Reflect.api.DataInterface.extend({
      	init: function(config) {
      		this._super(config);
      		this.api_loc = Drupal.settings.reflect.servicesPath;
      	},
       	get_templates: function(callback) {
       		$j.get(Drupal.settings.reflect.reflectPath + '/templates/templates.html', callback);
       	},
       	get_current_user: function() {
    		  var user = $j.trim($j('#comment-form .username').text());
    		  if(!user || user == '')
    			user = 'Anonymous';
      		return user;
       	},
      	post_bullet: function(settings){ 
          if(settings.params.bullet_id) {
            var url =  Drupal.settings.reflect.servicesPath + 'bullet/' + settings.params.bullet_id + '.json';
            if(settings.params['delete']) {
              var type = 'DELETE';
            } else {
              var type = 'PUT';
            }
          } 
          else {
            var url =  Drupal.settings.reflect.servicesPath + 'bullet.json';
            var type = 'POST';
          }
  
          $j.ajax({
            url: url,
            type: type,
            data: settings.params,
            error: function(data){
                settings.error(data);
            },
            success: function(data){
                settings.success(data);
            }
          });
        },
        post_rating: function(settings){ 

          var url =  Drupal.settings.reflect.servicesPath + 'rate.json';
          var type = 'POST';
  
          $j.ajax({
            url: url,
            type: type,
            data: settings.params,
            async: true,
            error: function(data){
                settings.error(data);
            },
            success: function(data){
                settings.success(data);
            }
          });
        },        
      	post_response: function(settings){
          if(settings.params.response_id){
              var url = Drupal.settings.reflect.servicesPath + 'response/' + settings.params.response_id + '.json';
              if(settings.params['delete']) {
                var type = 'DELETE';
              } else {
                var type = 'PUT';
              }
          } 
          else {
              var url = Drupal.settings.reflect.servicesPath + 'response.json';
              var type = 'POST';
          }
        	$j.ajax({
            url: url,
            type:type,
            data: settings.params,
            error: function(data){
                settings.error(data);
            },
            success: function(data){
                settings.success(data);
            }
        	});
        },
        get_data: function(params, callback){
          $j.getJSON(Drupal.settings.reflect.servicesPath + 'bullet.json', params, callback);
      	}
      });
    }
  };

})(jQuery);