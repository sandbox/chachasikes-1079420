<?php
// $Id$

/**
 * @file
 */

function __get_user_info() {
  global $user;
  $user_name = $user->name;
  $uid = $user->uid;
  if (!$user_name || $user_name == '') {
    $user_name = 'Anonymous';
  }
  return (object) array('name' => $user_name, 'id' => $uid, 'is_anon' => !user_is_logged_in());
}

/**
 * Callback for getting reflect data.
 *
 * @param array $comments
 * @return array
 */
function get_data($comments) {
  $data = array();
  $user_info = __get_user_info();

  foreach (drupal_json_decode($comments) as $comment_id) {
    $bullets = array();
    $db_bullets = db_select('reflect_bullet_revision')
      ->fields('reflect_bullet_revision', array('bullet_id', 'id', 'created', 'user', 'text', 'rating_zen', 'rating_gold', 'rating_sun', 'rating_troll', 'rating_graffiti', 'rating'))
      ->condition('active', 1)
      ->condition('comment_id', $comment_id)
      ->execute()
      ->fetchAll();

    foreach ($db_bullets as $db_bullet) {

      $bullet = (object)array(
        'id' => $db_bullet->bullet_id,
        'rev' => $db_bullet->id,
        'ts' => $db_bullet->created,
        'u' => $db_bullet->user,
        'txt' => $db_bullet->text,
        'ratings' => array(
          'zen' => $db_bullet->rating_zen,
          'gold' => $db_bullet->rating_gold,
          'sun' => $db_bullet->rating_sun,
          'troll' => $db_bullet->rating_troll,
          'graffiti' => $db_bullet->rating_graffiti,
          'rating' => $db_bullet->rating
        )

      );

      if (!$user_info->is_anon){
        $db_ratings = db_select('reflect_bullet_rating')
          ->fields('reflect_bullet_rating', array('bullet_id', 'rating')) 
          ->condition('comment_id', $comment_id)
          ->condition('user_id', $user_info->id)
          ->execute()
          ->fetchAll();

        foreach ($db_ratings as $db_rating) {
          if ($db_rating->bullet_id == $db_bullet->bullet_id) {
            $bullet->my_rating = $db_rating->rating;
            $bullet->ratings[$db_rating->rating] -= 1; 
          }
        }
      }
      $highlights = db_select('reflect_highlight')
        ->fields('reflect_highlight', array('element_id'))
        ->condition('bullet_rev', $bullet->rev)
        ->execute()
        ->fetchAll();
      $bullet->highlights = array();
      foreach ($highlights as $highlight) {
        $bullet->highlights[] = $highlight->element_id;
      }
      $db_response = db_select('reflect_response_revision')
        ->fields('reflect_response_revision', array('response_id', 'id', 'created', 'user', 'text', 'signal'))
        ->condition('active', 1)
        ->condition('bullet_id', $bullet->id)
        ->execute()
        ->fetchObject();
      
      $bullet->response = $db_response ? array(
          'id' => $db_response->response_id,
          'rev' => $db_response->id,
          'ts' => $db_response->created,
          'sig' => $db_response->signal,
          'txt' => $db_response->text,
          'u' => $db_response->user,
        ) : Null;

      $bullets[$bullet->id] = $bullet;
    }
    $data[$comment_id] = $bullets;
  }
  return $data;
}


//////////////////
// API for Bullets
//////////////////


function create_bullet($comment_id, $text, $highlights) {

  $user_info = __get_user_info();

  if ($text == '') return '';

  $bullet_params = array(
    'comment_id' => $comment_id,
  );

  $bullet_id = db_insert('reflect_bullet')->fields($bullet_params)->execute();

  $bullet_revision_params = array(
    'comment_id' => $comment_id,
    'bullet_id' => $bullet_id,
    'text' => $text,  // need to escape this, or it done automatically by drupal middleware?
    'user' => $user_info->name,
    'user_id' => $user_info->id,
    'active' => 1,
    'created' => date(DATE_ATOM, mktime(0, 0, 0, 7, 1, 2000)),
  );
  $bullet_rev_id = db_insert('reflect_bullet_revision')->fields($bullet_revision_params)->execute();

  foreach (drupal_json_decode($highlights) as $value) {
    $highlight_params = array(
      'bullet_id' => $bullet_id,
      'bullet_rev' => $bullet_rev_id,
      'element_id' => $value['eid'],
    );
    db_insert('reflect_highlight')->fields($highlight_params)->execute();
  }

  // send notification email to commenter, if possible
  if (variable_get('reflect_notify_commenter_of_summary', TRUE)) {
    global $base_url;
    $comment = comment_load($comment_id);  
    if ($comment->uid) {
      $account = user_load($comment->uid);
      $email = $account->mail;
    } else {
      $email = $comment->mail;
    }
    if ($email) {
      $node = node_load($comment->nid);
      $params = array(
        "user" => $user_info->name,
        "post_title" => $node->title,
        "comment_author" => $comment->uid ? $comment->registered_name : $comment->name,
        "bullet_text" => $text, // TODO: sanitize this
        "link" => url('node/' . $node->nid, array('absolute' => TRUE, 'fragment' => "comment-" . $comment->cid))
      );
      if ($comment->uid) { 
        $params['account'] = $account;
        $pref_lang = user_preferred_language($account);
      } else {
        $pref_lang = user_preferred_language($user); //default to current user if commenter didn't have account
      }
      drupal_mail('reflect', 'your comment summarized', $email, $pref_lang, $params);
    }
  }
  //////
  
  return (object)array(
    "insert_id" => $bullet_id,
    "u" => $user_info->name,
    "rev_id" => $bullet_rev_id
  );
}

function update_bullet($comment_id, $bullet_id, $text, $highlights) {
  $user_info = __get_user_info();

  if ($text == '') return '';

  db_query('UPDATE {reflect_bullet_revision} SET active=0 WHERE bullet_id=' . $bullet_id);

  $bullet_revision_params = array(
    'comment_id' => $comment_id,
    'bullet_id' => $bullet_id,
    'text' => $text,  // need to escape this, or it done automatically by drupal middleware?
    'user' => $user_info->name,
    'user_id' => $user_info->id,
    'active' => 1,
    'created' => date(DATE_ATOM, mktime(0, 0, 0, 7, 1, 2000)),
  );
  $bullet_rev_id = db_insert('reflect_bullet_revision')->fields($bullet_revision_params)->execute();

  foreach (drupal_json_decode($highlights) as $value) {
    $highlight_params = array(
      'bullet_id' => $bullet_id,
      'bullet_rev' => $bullet_rev_id,
      'element_id' => $value['eid'],
    );
    db_insert('reflect_highlight')->fields($highlight_params)->execute();
  }

  return (object)array(
    "insert_id" => $bullet_id,
    "u" => $user_info->name,
    "rev_id" => $bullet_rev_id
  );
}

function delete_bullet($bullet_id) {
  db_query('UPDATE {reflect_bullet_revision} SET active=0 WHERE bullet_id=' . $bullet_id);
}


function create_bullet_rating($comment_id, $bullet_id, $bullet_rev, $rating, $is_delete) {
  $user_info = __get_user_info();
  $uid = $user_info->id;
  
  #server side permission check for this operation...
  #my $commenter = $slashdb->sqlSelect('uid', 'comments', "cid = $comment_id");
  #my $summarizer = $slashdb->sqlSelect('user_id', 'reflect_bullet_revision', "id = $bullet_rev");
  #if($commenter == $uid
  #   || $user_info->{is_anon}
  #   || $summarizer == $uid ) {
  #  return "rejected";
  #}
  db_delete('reflect_bullet_rating')
    ->condition('user_id', $uid)
    ->condition('bullet_id', $bullet_id)
    ->execute();

  if($is_delete == 'false') {
    $rating_params = array( 
        'comment_id' => $comment_id,
        'bullet_id' => $bullet_id,
        'bullet_rev' => $bullet_rev,
        'rating' => $rating,
        'user_id' => $uid,
        'created' => date(DATE_ATOM, mktime(0, 0, 0, 7, 1, 2000)),
      );
    db_insert('reflect_bullet_rating')
      ->fields($rating_params)
      ->execute();
  }

  $ratings = db_query('SELECT rating, count(*) as cnt FROM {reflect_bullet_rating} WHERE bullet_id='.$bullet_id.' GROUP BY rating');
    #->fields('reflect_bullet_rating', array('rating',))
    #->addExpression('COUNT(*)', 'cnt')
    #->condition('bullet_id', $bullet_id)
    #->groupBy('rating')
    #->execute()
    #->fetchAll();
    
  $update_obj = array(
    'rating_zen' => 0,
    'rating_gold' => 0,
    'rating_sun' => 0,
    'rating_troll' => 0,
    'rating_graffiti' => 0
  );
  $high_cnt = 0;
  foreach ($ratings as $row) {
    $row_rating = $row->rating;
    $update_obj["rating_" . $row_rating] = $row->cnt;
    if($row->cnt > $high_cnt){
      $high_cnt = $row->cnt;
      $high_rating = $row->rating;
    }
  }
  
  $update_obj["rating"] = $high_cnt > 0 ? $high_rating : Null;

  db_update('reflect_bullet_revision')
    ->fields($update_obj)
    ->condition('bullet_id', $bullet_id)
    ->condition('active', 1)
    ->execute();

  ### determine if we should deactivate this bullet  
  #my $deactivate = 'false';
  #if( ( !$is_delete && ( $rating == 'troll' || $rating == 'graffiti' ) 
  #  || ( $is_delete && ( $rating != 'troll' && $rating != 'graffiti' )))) {    

  #  my $good_ratings = $update_obj->{'rating_zen'} + $update_obj->{'rating_gold'} + $update_obj->{'rating_sun'};
  #  my $bad_ratings = $update_obj->{'rating_troll'} + $update_obj->{'rating_graffiti'};
  #  my $total_count = $good_ratings + $bad_ratings;
  #  my ($rev_id, $signal) = $slashdb->sqlSelect(
  #    'id, signal',
  #    'reflect_response_revision',
  #    'active=1 AND bullet_id=' . $bullet_id
  #  );
    
  #  if ( ($total_count >= 3 && $good_ratings < $bad_ratings && !$signal) 
  #    || ( $rev_id && $signal == 0 ) ) {
  #    $slashdb->sqlUpdate(
  #      'reflect_bullet_revision',
  #      {'active' => 0},
  #      "bullet_id=$bullet_id AND active=1"
  #    );
  #    $deactivate = 'true';
  #    _remove_messages_about_bullet($bullet_id);
  #  }
  #}

  # return "{\"rating\":\"$high_rating\",\"deactivate\":$deactivate}";
  return (object)array(
    "rating" => $high_rating,
    "deactivate" => false
  );
}



////////////////////
// API for Responses
////////////////////

function create_response($comment_id, $bullet_id, $text, $signal) {

  $user_info = __get_user_info();

  $response_params = array(
    'comment_id' => $comment_id,
    'bullet_id' => $bullet_id
  );

  $response_id = db_insert('reflect_response')->fields($response_params)->execute();

  $response_revision_params = array(
    'comment_id' => $comment_id,
    'bullet_id' => $bullet_id,
    'response_id' => $response_id,
    'text' => $text,  // need to escape this, or it done automatically by drupal middleware?
    'user' => $user_info->name,
    'user_id' => $user_info->id,
    'signal' => $signal,
    'active' => 1,
    'created' => date(DATE_ATOM, mktime(0, 0, 0, 7, 1, 2000)),
  );
  $response_rev_id = db_insert('reflect_response_revision')->fields($response_revision_params)->execute();

  // send notification email to summarizer, if possible
  if (variable_get('reflect_notify_summarizer_of_response', TRUE)) {
    global $base_url;
    $bullet = bullet_load($bullet_id);  
    if ($bullet->user_id) {
      $account = user_load($bullet->user_id);
      $email = $account->mail;
      $comment = comment_load($comment_id);
      $node = node_load($comment->nid);
      $params = array(
        "user" => $user_info->name,
        "post_title" => $node->title,
        "bullet_author" => $account->name,
        "bullet_text" => $bullet->text,
        "response_text" => $text, // TODO: sanitize this
        "link" => url('node/' . $node->nid, array('absolute' => TRUE, 'fragment' => "comment-" . $comment->cid)),
        "signal" => $signal
      );
      $params['account'] = $account;
      $pref_lang = user_preferred_language($account);
      drupal_mail('reflect', 'your summary responded to', $email, $pref_lang, $params);
    }
  }
  //////

  return (object)array(
    "insert_id" => $response_id,
    "u" => $user_info->name,
    "rev_id" => $response_rev_id,
    "sig" => $signal
  );
}

function update_response($comment_id, $bullet_id, $response_id, $text, $signal ) {
  $user_info = __get_user_info();

  db_query('UPDATE {reflect_response_revision} SET active=0 WHERE response_id=' . $response_id);

  $response_revision_params = array(
    'comment_id' => $comment_id,
    'bullet_id' => $bullet_id,
    'response_id' => $response_id,
    'text' => $text,  // need to escape this, or is it done automatically by drupal middleware?
    'user' => $user_info->name,
    'user_id' => $user_info->id,
    'signal' => $signal,
    'active' => 1,
    'created' => date(DATE_ATOM, mktime(0, 0, 0, 7, 1, 2000)),
  );
  $response_rev_id = db_insert('reflect_response_revision')->fields($response_revision_params)->execute();

  return (object)array(
    "insert_id" => $response_id,
    "u" => $user_info->name,
    "rev_id" => $response_rev_id,
    "sig" => $signal
  );
}

function delete_response($response_id) {
  db_query('UPDATE {reflect_response_revision} SET active=0 WHERE response_id=' . $response_id);
}