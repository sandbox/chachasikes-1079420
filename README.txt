This module provides integration between the Reflect project 
(by Travis Kriplean @hotvig) and Drupal.
http://www.cs.washington.edu/homes/travis/reflect/

Crowdsourced comment summarization. Helps people listen. 
Helps everyone find the useful points.

Dev Notes
* Create a Drupal 7 dev site.
* Install reflect module, along with services and cTools.
* Enable Services, Services REST, cTools and reflect.

* You will need to add third party libraries.
* All of the files here: 
http://plugins.svn.wordpress.org/reflect/trunk/js/third_party/
(Note: jQuery and Pulse are not needed for this module.)
* Load files into the reflect/js/third_party folder.


* Works best with Bartik theme. 
@TODO Integrate better with themeing layer.



Testing Comments
To see Reflect in action.

1. Choose a content type that you would like Reflect to appear on.
2. In the settings, turn Reflect on.
3. Create a new node of that type.
4. Add a comment.
5. Login with a *different* user account (you can't summarize your own comments.)
6. On the comment, you should see controls for Reflect.

@TODO
Test that Services will actually load.

